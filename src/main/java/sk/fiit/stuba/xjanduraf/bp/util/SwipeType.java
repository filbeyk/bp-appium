package sk.fiit.stuba.xjanduraf.bp.util;

public enum SwipeType {
	LEFT_RIGHT,
	BOTTOM_TOP;
}
