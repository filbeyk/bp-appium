package sk.fiit.stuba.xjanduraf.bp.util;

public enum SwipeDuration {
	/**
	 * Swaps one page TODO verify if normal swipe depends on distance between swipe points
	 */
	NORMAL(0),
	/**
	 * Swaps more pages with one swap
	 */
	FAST(100), 
	
	SLOW(5000);
	
	SwipeDuration(int duration){
		this.setDuration(duration);
	}
	private int duration;

	
	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}
}
