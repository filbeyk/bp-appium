package sk.fiit.stuba.xjanduraf.bp.samples;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;
import sk.fiit.stuba.xjanduraf.bp.util.AbstractTest;
import sk.fiit.stuba.xjanduraf.bp.util.ActionDTO;

public class GriddlersTest extends AbstractTest {

	@Before
	public void setUp() throws Exception {
		// set up appium
		// final File classpathRoot = new File(System.getProperty("user.dir"));
		final File appDir = new File("/mnt/WINDOWS/ZDIELANE/Workspaces/workspaceAndroid/Griddlers/bin");
		final File app = new File(appDir, "Griddlers.apk");

		capabilities.setCapability("app", app.getAbsolutePath());
		capabilities.setCapability("appPackage", "sk.ynet.janko.griddlers");
		capabilities.setCapability("appActivity", ".mainTabs.MainActivity");
		// capabilities.setCapability("ignoreUnimportantViews", true);
		// http://appium.io/slate/en/master/?ruby#android-only

		createDriver();
	}

	@Test
	public void testGriddler() throws IOException {

		// open griddler one
		action = new ActionDTO("Griddler1Click", () -> {
			WebElement griddler1 = driverUtil.findElementInListThatContainsElementWithText("android.widget.TextView", "sk.ynet.janko.griddlers:id/gridview", "griddler1");
			griddler1.click();
		} );
		driverUtil.executeWithFatalError(action);

		int[] textViewIndexesToClick = new int[] { 17, 18, 19, 24, 25, 31, 32, 41, 42, 45, 46, 48, 49 };
		action = new ActionDTO("Fill Griddler", () -> {
			List<WebElement> textViews = driver.findElements(By.xpath("//android.widget.TextView"));
			for (Integer index : textViewIndexesToClick) {
				textViews.get(index-1+4).click();; //because list is zero based and 4 for previous textViews
			}
		} );
		driverUtil.executeWithFatalError(action);
		
		driver.sendKeyEvent(AndroidKeyCode.MENU);
		AndroidElement anElement = (AndroidElement) driverUtil.findElementThatContainsCaseInsensitiveText("reset");
//		anElement.getAttribute(name);getCssValue(propertyName)


//
//		driver.sendKeyEvent(AndroidKeyCode.MENU);
//
//		// WebElement test = driverUtil.findElementThatContainsCaseInsensitiveText("aDelO");

	}
	
	@Test
	public void testTimepickerAndSpinner(){
		 WebElement solve = driverUtil.findElementByStringId("solve");
		 solve.click();
		 
		 WebElement spinnerTextView = driverUtil.findDescendantElement("android.widget.Spinner", "sk.ynet.janko.griddlers:id/spinner", "android.widget.TextView");
		 spinnerTextView.click();
		 WebElement italy = driverUtil.findElementInListThatContainsElementWithText("android.widget.CheckedTextView", null, "Ital");
		 italy.click();
		 WebElement timePicker = driver.findElementById("sk.ynet.janko.griddlers:id/timePicker");
	}
	
	@Test
	public void testInputControls(){
		// editext ->different input types??
		// autocomplete text view
		// multiautocomplete textview
		WebElement multiAutocomplete = driver.findElement(By.id("sk.ynet.janko.griddlers:id/multiAutoCompleteTextView"));
		multiAutocomplete.sendKeys("Fra");
		// checkbox
		WebElement checkbox = driver.findElement(By.id("sk.ynet.janko.griddlers:id/checkbox"));
		checkbox.click();
		boolean isSelected = Boolean.parseBoolean(checkbox.getAttribute("checked"));

		// radiobutton
		WebElement radio = driver.findElement(By.id("sk.ynet.janko.griddlers:id/radio0"));
		radio.click();
		isSelected = Boolean.parseBoolean(radio.getAttribute("checked"));
		radio.getAttribute("checked");

		// togglebutton
		WebElement togglebutton = driver.findElement(By.id("sk.ynet.janko.griddlers:id/togglebutton"));
		togglebutton.click();
		isSelected = Boolean.parseBoolean(togglebutton.getAttribute("checked")); // not working isEnabled also not working
		togglebutton.getAttribute("checked");
	}

	public static String caseInsensitiveXpathTextCondition(String value) {
		return "translate(normalize-space(@text),'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=" + escapeQuotes(value.toUpperCase());
	}

	private static String escapeQuotes(String toEscape) {
		return String.format("\"%s\"", toEscape);
	}
}
