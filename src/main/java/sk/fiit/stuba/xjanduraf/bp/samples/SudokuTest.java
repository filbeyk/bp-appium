package sk.fiit.stuba.xjanduraf.bp.samples;

import java.io.File;
import java.net.URL;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import sk.fiit.stuba.xjanduraf.bp.util.AbstractTest;
import sk.fiit.stuba.xjanduraf.bp.util.SelectionUtil;

public class SudokuTest extends AbstractTest{


	@Before
	public void setUp() throws Exception {
		// set up appium
		// final File classpathRoot = new File(System.getProperty("user.dir"));
		final File appDir = new File("/mnt/WINDOWS/ZDIELANE/owncloud/Bakalarka/Demo_Apps/OpenSudoku/bin");
		final File app = new File(appDir, "OpenSudoku.apk");

		capabilities.setCapability("app", app.getAbsolutePath());
		capabilities.setCapability("appPackage", "cz.romario.opensudoku");
		capabilities.setCapability("appActivity", ".gui.FolderListActivity");
		// capabilities.setCapability("ignoreUnimportantViews", true);
		// http://appium.io/slate/en/master/?ruby#android-only

		// click close button on new changes view
		WebElement el = driver.findElement(By.className("android.widget.Button"));
		el.click();
	}

	@SuppressWarnings("unused")
	 @Test
	public void addContact() {
		// finding medium textview element
		// vsade kde je get(1) nie az tak vhodne v pripade pridanie podobneho elementu pred hladany moze vratit chybny element
		// id je celkom fajn v pripade ze sa casto pocas vyvoja nemenia elementy a ich samotne ids
		final List<WebElement> id = driver.findElements(By.id("cz.romario.opensudoku:id/name")); // get(1)

		final List<WebElement> name = driver.findElements(By.name("Medium")); // get(0)
		final List<WebElement> className = driver.findElements(By.className("android.widget.TextView")); // get(1)
		final List<WebElement> xpathByName = driver.findElements(By.xpath("//*[@text='Medium']"));// get(0)
		final List<WebElement> xpathById = driver.findElements(By.xpath("//*[@resource-id='cz.romario.opensudoku:id/name']"));
		final List<WebElement> xpathByClassName = driver.findElements(By.xpath("//android.widget.TextView"));


		final List<WebElement> xpath1 = driver.findElements(By
				.xpath("//android.widget.TextView[@resource-id='cz.romario.opensudoku:id/name' and @text='Medium']"));// get(0)
		final List<WebElement> xpath = driver.findElements(By
				.xpath("//android.widget.TextView[@resource-id='cz.romario.opensudoku:id/name']"));// get(1)
		// final List<WebElement> el1 = driver.findElements(By.tagName("TextView")); not supported
		// driver.findElements(By.linkText(linkText)); not supported
		// driver.findElements(By.partialLinkText(linkText)); not supported
		// driver.findElements(By.cssSelector("textView")); not supported
		// http://appium.wikia.com/wiki/Finding_Elements

	}

	@Test
	public void addNewFolderTest() {
		// ResourceActivity act = new ResourceActivity();
		// Resources res = act.getResources();
		// String value = res.getString(R.string.add_sudoku);
		driver.sendKeyEvent(AndroidKeyCode.MENU);
		// By.name case sensitive ->lepsie cez xpath (da sa spravit case insensitive) TODO
		// driver.findElement(By.name("Add Folder")).click();; padne
		By addFolderBy = By.xpath("//*[" + SelectionUtil.caseInsensitiveXpathCondition("add folder") + "]");
		driver.findElement(By.name("Add folder")).click();
		; // get(1)
			// fist find textview by its name and after that locate edittext (TextView Name: pravdepodobne bude spojene so svojim edittextom -> preto je
			// vyhodnejsie
			// vyhladavat cez neho -> v pripade zmeny mena sa moze zmenit aj label)
			// label uchovat ako String a sem ako text pouzit label zo Stringu -> (maven appka) test v rovnakom projekte ako appka
		WebElement inputField = driver.findElement(By
				.xpath("//android.widget.TextView[@text='Name:']/following-sibling::android.widget.EditText"));
		inputField.sendKeys("New Folder");
		// driver.findElement(By.className("android.widget.EditText")); co ak sa prida dalsi edit text element???
		// driver.findElement(By.id("cz.romario.opensudoku:id/name")); defaultne id -> pri pridani dalsieho edit textu moze padnut
		driver.findElement(By.name("Save")).click();

	}
}
