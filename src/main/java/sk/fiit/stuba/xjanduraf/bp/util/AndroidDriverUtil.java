package sk.fiit.stuba.xjanduraf.bp.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import com.asprise.ocr.Ocr;

import io.appium.java_client.android.AndroidDriver;

public class AndroidDriverUtil {

	private static final Logger LOGGER = Logger.getLogger(AndroidDriverUtil.class.getSimpleName());

	public static String APPIUM_HOME = "/home/janko/git/appium";
	public static String TEST_REPORT_DIR = "reports/";

	private String createTime;
	private String reportSubDirectory;
	private AndroidDriver androidDriver;
	private Ocr ocr;
	private List<ActionDTO> actions;

	public AndroidDriverUtil(AndroidDriver androidDriver) {
		createTime = getCurrentTime();
		this.androidDriver = androidDriver;
		String appPackage = (String) androidDriver.getCapabilities().getCapability("appPackage");
		if (appPackage == null) {
			appPackage = androidDriver.getSessionId().toString();
		} else {
			String[] splittedAppPackage = appPackage.split("\\.");
			appPackage = splittedAppPackage[splittedAppPackage.length - 1];
		}
		reportSubDirectory = TEST_REPORT_DIR + appPackage + File.separator + createTime + File.separator;
		Ocr.setUp();
	}

	public AndroidDriverUtil(AndroidDriver androidDriver, List<ActionDTO> actions) {
		this(androidDriver);
		this.actions = actions;
	}

	private String getCurrentTime() {
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	public String getStringById(String stringId) {
		String jsonAppStrings = getAppStringInJson();
		JSONObject appStrings = new JSONObject(jsonAppStrings);
		String result = appStrings.getString(stringId);
		return result;
	}

	private String getAppStringInJson() {
		return getAppStringInJson("");
	}

	private String getAppStringInJson(String locale) {
		Process getStringProcess;
		StringBuilder jsonString = new StringBuilder();
		try {
			String pathToApk = (String) androidDriver.getCapabilities().getCapability("app");
			String packageName = (String) androidDriver.getCapabilities().getCapability("appPackage");
			new ProcessBuilder("java", "-jar", APPIUM_HOME + "/node_modules/appium-adb/jars/appium_apk_tools.jar", "stringsFromApk", pathToApk, "/tmp/" + packageName,
					locale).start();
			Thread.sleep(1000);
			getStringProcess = new ProcessBuilder("cat", "/tmp/" + packageName + "/strings.json").start();
			InputStreamReader isr = new InputStreamReader(getStringProcess.getInputStream());
			BufferedReader br = new BufferedReader(isr);
			String line;
			while ((line = br.readLine()) != null) {
				jsonString.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonString.toString();
	}

	public WebElement findElementInActionBar(String string) {
		WebElement uiElement = androidDriver.findElement(By.xpath("//*[@resource-id='android:id/action_bar']/descendant::*[@text='" + string + "']"));
		return uiElement;
	}

	public WebElement findElementByTextCaseInsensitive(String text) {
		By caseInsenstiveTextBy = By.xpath("//*[" + SelectionUtil.caseInsensitiveXpathCondition(text) + "]");
		WebElement uiElement = androidDriver.findElement(caseInsenstiveTextBy);
		return uiElement;
	}

	public WebElement findElementThatContainsText(String text) {
		By containsTextBy = By.xpath("//*[" + SelectionUtil.containsCondition(text) + "]");
		WebElement uiElement = androidDriver.findElement(containsTextBy);
		return uiElement;
	}

	public WebElement findElementThatContainsCaseInsensitiveText(String text) {
		By containsTextBy = By.xpath("//*[" + SelectionUtil.containsCaseInsensitiveXPathCondition(text) + "]");
		WebElement uiElement = androidDriver.findElement(containsTextBy);
		return uiElement;
	}

	public WebElement findElementByStringId(String stringId) {
		WebElement uiElement = androidDriver.findElement(By.xpath("//*[@text='" + getStringById(stringId) + "']"));
		return uiElement;
	}

	public WebElement findFollowingElement(String rootElementText, String elementsToFindClassName, int index) {
		List<WebElement> followingElements = androidDriver.findElements(By.xpath("//*[@text='" + rootElementText + "']/following-sibling::" + elementsToFindClassName));
		return followingElements.get(index);
	}

	public WebElement findFollowingElement(String rootElementText, String elementsToFindClassName) {
		return findFollowingElement(rootElementText, elementsToFindClassName, 0);
	}

	public WebElement findDescendantElement(String rootClassName, String descendantOrSelfTextorId, String elementToFindClassName) {
		WebElement uiElement = androidDriver.findElement(By.xpath("//" + rootClassName + "[descendant-or-self::*[@text='" + descendantOrSelfTextorId
				+ "' or @resource-id='" + descendantOrSelfTextorId + "']]/" + elementToFindClassName));
		return uiElement;
	}

	public WebElement findElementInListThatContainsElementWithText(String elementClass, String listId, String text) {
		return findElementInAbsListThatContainsElementWithText(elementClass, listId, text, SwipeDuration.NORMAL);
	}

	public WebElement findElementInAbsListThatContainsElementWithText(String elementClass, String listId, String text, SwipeDuration duration) {
		String absListId = "";
		if (!StringUtils.isEmpty(listId)) {
			absListId = " and @resource-id='" + listId + "'";
		}
		String listXPath = "//*[android.widget.ListView or android.widget.GridView" + absListId + "]";
		swipeFastToTop(androidDriver.findElement(By.xpath(listXPath)));
		boolean scrollDownPerformed = true;
		Exception noSuchElement = null;
		while (scrollDownPerformed) {
			try {
				WebElement uiElement = androidDriver.findElement(By.xpath(
						listXPath + "/descendant::" + elementClass + "[descendant-or-self::*[" + SelectionUtil.containsCaseInsensitiveXPathCondition(text) + "]]"));
				return uiElement;
			} catch (Exception e) {
				noSuchElement = e;
				scrollDownPerformed = swipeDownAbsListViewPage(androidDriver.findElement(By.xpath(listXPath)), duration);
			}
		}
		throw new IllegalArgumentException(getErrorMessage(elementClass, listId, text, noSuchElement));
	}

	private String getErrorMessage(String elementClass, String listId, String text, Exception noSuchElement) {
		return "Cannot find element with elementClass: '" + elementClass + "', listId: '" + listId + "', and text: '" + text + "'. Error: " + noSuchElement.getMessage();
	}

	/**
	 * 
	 * @param listId
	 * @param childrenClassName
	 * @param index
	 *            zero based
	 * @param descendantChildrenElementClassName
	 * @return
	 */
	public WebElement getElementFromList(String listId, String childrenClassName, int index, String descendantChildrenElementClassName) {
		WebElement uiElement = androidDriver.findElement(By.xpath("//*[@resource-id='" + listId + "']/" + childrenClassName + "[position() = " + (index + 1)
				+ " ]/descendant-or-self::" + descendantChildrenElementClassName));
		return uiElement;
	}

	/**
	 * Return true if swipe was possible, otherwise return false
	 * 
	 * @param absListView
	 * @return
	 */
	public boolean swipeDownAbsListViewPage(WebElement absListView) {
		return swipeDownAbsListViewPage(absListView, SwipeDuration.NORMAL);
	}

	public boolean swipeDownAbsListViewPage(WebElement absListView, SwipeDuration duration) {
		Runnable swipeRunnable = () -> {
			SwipeDTO swipe = getBottomTopSwipeDTOFromAbsListView(absListView);
			int swapDuration = getSwipeDuration(swipe.getBottomY() - swipe.getTopY(), duration);
			androidDriver.swipe(swipe.getBottomX(), swipe.getBottomY(), swipe.getTopX(), swipe.getTopY(), swapDuration);
		};
		return swipe(absListView, duration, swipeRunnable);
	}

	public boolean containsItemWithText(List<WebElement> listItems, String text) {
		for (WebElement uiElement : listItems) {
			if (uiElement.getText().equalsIgnoreCase(text)) {
				return true;
			}
		}
		return false;
	}

	private int getSwipeDuration(int swipeDistance, SwipeDuration duration) {
		int swapDuration;
		if (SwipeDuration.NORMAL.equals(duration)) {
			swapDuration = 4 * swipeDistance;
		} else {
			swapDuration = duration.getDuration();
		}
		return swapDuration;
	}

	/**
	 * Return true if swipe was possible, otherwise return false
	 * 
	 * @param absListView
	 * @return
	 */
	public boolean swipeUpAbsListViewPage(WebElement absListView) {
		return swipeUpAbsListViewPage(absListView, SwipeDuration.NORMAL);
	}

	public boolean swipeUpAbsListViewPage(WebElement absListView, SwipeDuration duration) {
		Runnable swipeRunnable = () -> {
			SwipeDTO swipe = getBottomTopSwipeDTOFromAbsListView(absListView);
			int swapDuration = getSwipeDuration(swipe.getBottomY() - swipe.getTopY(), duration);
			androidDriver.swipe(swipe.getTopX(), swipe.getTopY(), swipe.getBottomX(), swipe.getBottomY(), swapDuration);
		};
		return swipe(absListView, duration, swipeRunnable);
	}

	public boolean swipeRightViewPage(WebElement viewPager, SwipeDuration duration) {
		Runnable swipeRunnable = () -> {
			SwipeDTO swipe = getLeftRightSwipeDTOFromAbsListView(viewPager);
			int swapDuration = getSwipeDuration(swipe.getRightX() - swipe.getLeftX(), duration);
			androidDriver.swipe(swipe.getRightX(), swipe.getRightY(), swipe.getLeftX(), swipe.getLeftY(), swapDuration);
		};
		return swipe(viewPager, duration, swipeRunnable);
	}

	private boolean swipe(WebElement viewPager, SwipeDuration duration, Runnable swipe) {
		String pageSourceBeforeSwipe = androidDriver.getPageSource();
		swipe.run();
		String pageSourceAfterSwipe = androidDriver.getPageSource();
		boolean swipeImpossible = pageSourceAfterSwipe.equals(pageSourceBeforeSwipe);
		return !swipeImpossible;
	}

	public boolean swipeLeftViewPage(WebElement viewPager, SwipeDuration duration) {
		Runnable swipeRunnable = () -> {
			SwipeDTO swipe = getLeftRightSwipeDTOFromAbsListView(viewPager);
			int swapDuration = getSwipeDuration(swipe.getRightX() - swipe.getLeftX(), duration);
			androidDriver.swipe(swipe.getLeftX(), swipe.getLeftY(), swipe.getRightX(), swipe.getRightY(), swapDuration);
		};
		return swipe(viewPager, duration, swipeRunnable);
	}

	public void swipeFastToBottom(WebElement absListView) {
		boolean swipePossible = true;
		while (swipePossible) {
			swipePossible = swipeDownAbsListViewPage(absListView, SwipeDuration.FAST);
		}
	}

	public void swipeFastToTop(WebElement absListView) {
		boolean swipePossible = true;
		while (swipePossible) {
			swipePossible = swipeUpAbsListViewPage(absListView, SwipeDuration.FAST);
		}
	}

	public void swipeFastToRight(WebElement viewPager) {
		boolean swipePossible = true;
		while (swipePossible) {
			swipePossible = swipeRightViewPage(viewPager, SwipeDuration.FAST);
		}
	}

	public void swipeFastToLeft(WebElement viewPager) {
		boolean swipePossible = true;
		while (swipePossible) {
			swipePossible = swipeLeftViewPage(viewPager, SwipeDuration.FAST);
		}
	}

	public SwipeDTO getBottomTopSwipeDTOFromAbsListView(WebElement absListView) {
		Point absListViewLocation = absListView.getLocation();
		Dimension absListViewSize = absListView.getSize();
		int topX = absListViewLocation.x + absListViewSize.width / 2;
		int topY = absListViewLocation.y + 1;
		int bottomX = topX;
		int bottomY = absListViewLocation.y + absListViewSize.height - 1;
		SwipeDTO downUpSwipe = new SwipeDTO(SwipeType.BOTTOM_TOP);
		downUpSwipe.setBottomX(bottomX);
		downUpSwipe.setBottomY(bottomY);
		downUpSwipe.setTopX(topX);
		downUpSwipe.setTopY(topY);
		return downUpSwipe;
	}

	private SwipeDTO getLeftRightSwipeDTOFromAbsListView(WebElement viewPager) {
		Point viewPagerLocation = viewPager.getLocation();
		Dimension viewPagerSize = viewPager.getSize();
		int leftX = viewPagerLocation.x + 1;
		int leftY = viewPagerLocation.y + viewPagerSize.height / 2;
		int rightX = viewPagerLocation.x + viewPagerSize.width - 1;
		int rightY = leftY;
		SwipeDTO leftRightSwipe = new SwipeDTO(SwipeType.LEFT_RIGHT);
		leftRightSwipe.setLeftX(leftX);
		leftRightSwipe.setLeftY(leftY);
		leftRightSwipe.setRightX(rightX);
		leftRightSwipe.setRightY(rightY);
		return leftRightSwipe;
	}

	public void executeWithFatalError(ActionDTO action) {
		actions.add(action);
		try {
			action.getAction().run();
		} catch (Exception e) {
			handleFailure(action, e);
			Assert.fail("Quiting test after executing action: '" + action.getName() + "' with error: " + e.getMessage());
		}
		handleSuccessfulExecution(action);
	}

	public void execute(ActionDTO action) {
		actions.add(action);
		try {
			action.getAction().run();
		} catch (Exception e) {
			handleFailure(action, e);
			return;
		}
		handleSuccessfulExecution(action);
	}

	private void handleSuccessfulExecution(ActionDTO action) {
		String actionFile = action.getName() + "_" + action.getTimeStampId();
		String screenShot = makeScreenShot(actionFile + ".png");
		action.setScreenShot(screenShot);
		String pageSource = savePageSource(actionFile + ".xml");
		action.setPageSource(pageSource);
	}

	public void handleFailure(ActionDTO action, Exception e) {
		String actionFile = action.getName() + "_" + action.getTimeStampId();
		String errorLog = saveErrorLog(actionFile + ".log", e);
		action.setErrorLog(errorLog);
		String screenShot = makeScreenShot(actionFile + ".png");
		action.setScreenShot(screenShot);
		String pageSource = savePageSource(actionFile + ".xml");
		action.setPageSource(pageSource);
	}

	private String saveErrorLog(String errorLogName, Exception e) {
		String errorLog = e.getMessage();
		String errorLogPath = reportSubDirectory + errorLogName;
		try {
			FileUtils.write(new File(errorLogPath), errorLog);
		} catch (IOException exception) {
			LOGGER.warning("Could not save error log with path: " + errorLogPath + ". Error: " + exception.getMessage());
		}
		return errorLogPath;
	}

	public String savePageSource(String pageSourceName) {
		String pageSource = androidDriver.getPageSource();
		String pageSourcePath = reportSubDirectory + pageSourceName;
		try {
			FileUtils.write(new File(pageSourcePath), pageSource);
		} catch (IOException e) {
			LOGGER.warning("Could not save page soruce with path: " + pageSourcePath + ". Error: " + e.getMessage());
		}
		return pageSourcePath;
	}

	public String makeScreenShot(String screenShotName) {
		File screenshot = androidDriver.getScreenshotAs(OutputType.FILE);
		String screenShotPath = getScreenshotPath(screenShotName);
		try {
			FileUtils.copyFile(screenshot, new File(screenShotPath));
		} catch (IOException e) {
			LOGGER.warning("Could not create screenshot with path: " + screenShotPath + ". Error: " + e.getMessage());
		}
		return screenShotPath;
	}

	private String getScreenshotPath(String screenShotName) {
		return reportSubDirectory + screenShotName;
	}

	public String clickAndMakeScreenShot(WebElement elementToClick, String screenshotName) {
		Executors.newSingleThreadScheduledExecutor().schedule(() -> {
			this.makeScreenShot(screenshotName);
		} , 2, TimeUnit.SECONDS);
		elementToClick.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return getScreenshotPath(screenshotName);
	}

	public void verifyTextInScreenShot(String screenShotPath, String text) {
		File[] screenShot = { new File(screenShotPath) };
		ocr = new Ocr();
		ocr.startEngine("eng", Ocr.SPEED_SLOW);
		String ocrScreen = ocr.recognize(screenShot, Ocr.RECOGNIZE_TYPE_TEXT, Ocr.OUTPUT_FORMAT_PLAINTEXT);
		if (!StringUtils.containsIgnoreCase(ocrScreen, text)) {
			String toastErrorLogPath = screenShotPath + ".log";
			try {
				String errorMessage = "Image: " + screenShotPath + " does not contains text: '" + text + "'.\nContent of image:\n";
				FileUtils.write(new File(toastErrorLogPath), errorMessage + ocrScreen);
			} catch (IOException exception) {
				LOGGER.warning("Could not save error log with path: " + toastErrorLogPath + ". Error: " + exception.getMessage());
			}
		}
		ocr.stopEngine();
	}

	public void verifyTextInElement(WebElement doneTextview, String expectedText) {
		String presentedText = doneTextview.getAttribute("text");
		Assert.assertEquals("Expected text in Done textview is: '" + expectedText + "' but presented text is '" + presentedText + "'", expectedText, presentedText);

	}

	/**
	 * Verifies toast message according textInToast parameter and if toast message is no presented, screenshot will be saved in predefined report subfolder. 
	 * @param elementToClick
	 * @param screenShotName
	 * @param textInToast
	 */
	public void verifyToastText(WebElement elementToClick, String screenShotName, String textInToast) {
		String screenShotPath = clickAndMakeScreenShot(elementToClick, screenShotName);
		verifyTextInScreenShot(screenShotPath, textInToast);
	}
}
