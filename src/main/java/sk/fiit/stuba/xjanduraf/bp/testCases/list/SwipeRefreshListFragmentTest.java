package sk.fiit.stuba.xjanduraf.bp.testCases.list;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import sk.fiit.stuba.xjanduraf.bp.util.AbstractTest;
import sk.fiit.stuba.xjanduraf.bp.util.ActionDTO;
import sk.fiit.stuba.xjanduraf.bp.util.SwipeDuration;

public class SwipeRefreshListFragmentTest extends AbstractTest{

	@Before
	public void setUp() throws Exception {

		final File appDir = new File("/home/janko/Downloads/android-sdk/samples/android-21/ui/SwipeRefreshListFragment/Application/" + "build/outputs/apk");
		final File app = new File(appDir, "Application-debug.apk");
		// uncomment to install app to device
		capabilities.setCapability("app", app.getAbsolutePath());
		capabilities.setCapability("appPackage", "com.example.android.swiperefreshlistfragment");
		capabilities.setCapability("appActivity", ".MainActivity");
		createDriver();
	}

	@Test
	public void swipeRefreshListFragmentTest() throws IOException, InterruptedException {
		// Step 1
		action = new ActionDTO("Obrazovka", () -> {
			WebElement appName = driverUtil.findElementByTextCaseInsensitive("SwipeRefreshListFragment");
			Assert.assertNotNull(appName);
		} );
		driverUtil.executeWithFatalError(action);

		// Step 4.2
		// list should have ID that will not change during development
		String listId = "android:id/list";

		String descendantChildrenElementClassName = "android.widget.TextView";
		final String elementToFind = "Orla";
		action = new ActionDTO(elementToFind, () -> {
			//Step 2
			WebElement OrlaTextView = driverUtil.findElementInAbsListThatContainsElementWithText(descendantChildrenElementClassName, listId, elementToFind,
					SwipeDuration.SLOW);
			//Step 3
			OrlaTextView.click();
		} );
		driverUtil.execute(action);

//		changeContentOfList(listView);

		final String elementToFind2 = "Queso Fresco";
		action = new ActionDTO(elementToFind2, () -> {
			//Step 4
			WebElement quesoFrescoTextView = driverUtil.findElementInListThatContainsElementWithText(descendantChildrenElementClassName, listId, elementToFind2);
			quesoFrescoTextView.click();
		} );
		driverUtil.execute(action);
	}

	private void changeContentOfList(WebElement listView) {
		driverUtil.swipeUpAbsListViewPage(listView);
		driverUtil.swipeUpAbsListViewPage(listView);
	}

}
