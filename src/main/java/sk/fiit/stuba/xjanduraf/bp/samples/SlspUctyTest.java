package sk.fiit.stuba.xjanduraf.bp.samples;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.android.AndroidKeyCode;
import sk.fiit.stuba.xjanduraf.bp.util.AbstractTest;
import sk.fiit.stuba.xjanduraf.bp.util.SelectionUtil;

public class SlspUctyTest extends AbstractTest{
	
	@Before
	public void setUp() throws Exception {
		// set up appium
		// final File classpathRoot = new File(System.getProperty("user.dir"));
		final File appDir = new File("/home/janko/Downloads/externalApks");
		final File app = new File(appDir, "sk.sporoapps.accounts.apk");

		capabilities.setCapability("app", app.getAbsolutePath());
		capabilities.setCapability("appPackage", "sk.sporoapps.accounts");
		capabilities.setCapability("appActivity", ".activities.MainActivity");
		// capabilities.setCapability("ignoreUnimportantViews", true);
		// http://appium.io/slate/en/master/?ruby#android-only

	}

	@Test
	public void loginTest() throws IOException {
		
		//Step 1.1
		WebElement loginInfo = driverUtil.findElementByStringId("login_desc");
//		String loginText = "Login to Internetbanking of Slovenská Sporiteľňa";
//		WebElement loginInfo = androidDriverUtil.findElementThatContainsCaseInsensitiveText(loginText);
		Assert.assertNotNull(loginInfo);
		
		//Step 1.2
		String loginName = driverUtil.getStringById("login_name");
//		String loginName = androidDriverUtil.findElementThatContainsText("USER NAME OR ALIAS").getText();
		WebElement userNameOrAlias = driverUtil.findFollowingElement(loginName,"android.widget.EditText");
		userNameOrAlias.sendKeys("username_alias");
		
		//Step 1.3
		String loginPassword = driverUtil.getStringById("login_pass");
//		String loginPassword = androidDriverUtil.findElementByTextCaseInsensitive("PASSWORD").getText();
		WebElement password = driverUtil.findFollowingElement(loginPassword, "android.widget.EditText");
		password.sendKeys("password");
		
		//Step 1.4
		driver.sendKeyEvent(AndroidKeyCode.ENTER);
		
		//Toast cannot be verified so verify again loginInfo
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[" + SelectionUtil.containsCondition("Login to Internetbanking") + "]")));
		loginInfo = driverUtil.findElementThatContainsText("Login to Internetbanking");
		Assert.assertNotNull(loginInfo);
	}
}
