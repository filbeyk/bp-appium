package sk.fiit.stuba.xjanduraf.bp.testCases.list;

import java.io.File;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import sk.fiit.stuba.xjanduraf.bp.util.AbstractTest;
import sk.fiit.stuba.xjanduraf.bp.util.ActionDTO;

public class CustomChoiceListTest extends AbstractTest{
	
	// set up appium
	@Before
	public void setUp() throws Exception {
		final File appDir = new File("/home/janko/Downloads/android-sdk/samples/android-21/ui/CustomChoiceList/Application/" + "build/outputs/apk");
		final File app = new File(appDir, "Application-debug.apk");
		capabilities.setCapability("app", app.getAbsolutePath());
		capabilities.setCapability("appPackage", "com.example.android.customchoicelist");
		capabilities.setCapability("appActivity", ".MainActivity");
		createDriver();
	}

	@Test
	public void customChoiceListTest() {
		// Step 1
		action = new ActionDTO("Obrazovka", () -> {
			WebElement appName = driverUtil.findElementByTextCaseInsensitive("CustomChoiceList");
			Assert.assertNotNull(appName);
		} );
		driverUtil.executeWithFatalError(action);

		String absListViewId = "android:id/list";

		String descendantChildrenElementClassName = "android.widget.TextView";
		String ambertAction = "Ambert";
		action = new ActionDTO(ambertAction, () -> {
			// Step 2
			WebElement eleventhTextView = driverUtil.findElementInListThatContainsElementWithText(descendantChildrenElementClassName, absListViewId, ambertAction);
			// Step 3
			eleventhTextView.click();
		} );
		driverUtil.executeWithFatalError(action);

		String linearLayuoutCheckable = "android.widget.LinearLayout";
		action = new ActionDTO("Checked", () -> {
			WebElement imageView = driverUtil.findDescendantElement(linearLayuoutCheckable, ambertAction,"android.widget.ImageView");
			boolean isSelected = Boolean.parseBoolean(imageView.getAttribute("checked"));
//			Assert.assertEquals(true, isSelected);
		} );
		driverUtil.execute(action);
	}

}
