package sk.fiit.stuba.xjanduraf.bp.util;


public class SelectionUtil {

    public static String caseInsensitiveXpathCondition(String text) {
        return "translate(normalize-space(@text),'" + text.toLowerCase() + "','" + text.toUpperCase() + "')=" +
                escapeQuotes(text.toUpperCase());
    }

	private static String escapeQuotes(String toEscape) {
		return String.format("\"%s\"", toEscape);
	}
	
	public static String containsCondition(String text){
		return "contains(@text,'"+text+"')";
	}
	
	public static String containsCaseInsensitiveXPathCondition(String text){
		return "contains(translate(normalize-space(@text),'" + text.toLowerCase() + "','" + text.toUpperCase() + "')," +
                escapeQuotes(text.toUpperCase())+")";
	}
}
