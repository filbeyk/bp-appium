package sk.fiit.stuba.xjanduraf.bp.testCases.list;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import sk.fiit.stuba.xjanduraf.bp.util.AbstractTest;
import sk.fiit.stuba.xjanduraf.bp.util.ActionDTO;

public class AdapterTransitionTest extends AbstractTest {
	private static final int LIST_ITEMS_TO_TEST = 5;
	
	private static final String absListViewId = "com.example.android.adaptertransition:id/abs_list_view";

	private static final String descendantChildrenElementClassName = "android.widget.TextView";

	// set up appium
	@Before
	public void setUp() throws Exception {
		final File appDir = new File("/home/janko/Downloads/android-sdk/samples/android-21/ui/AdapterTransition/Application/" + "build/outputs/apk");
		final File app = new File(appDir, "Application-debug.apk");
		capabilities.setCapability("app", app.getAbsolutePath());
		capabilities.setCapability("appPackage", "com.example.android.adaptertransition");
		capabilities.setCapability("appActivity", ".MainActivity");
		createDriver();
	}

	@Test
	public void adapeterTransitionTest() {

		// Step 1
		// not good practise to access element by hard coded name (use string.xml id instead)
		action = new ActionDTO("Obrazovka", () -> {
			WebElement appName = driverUtil.findElementByTextCaseInsensitive("AdapterTransition");
			Assert.assertNotNull(appName);
		} );
		driverUtil.executeWithFatalError(action);
		String eleventhAction = "Eleventh";
		action = new ActionDTO(eleventhAction, () -> {
			// Step 2
			WebElement eleventhTextView = driverUtil.findElementInListThatContainsElementWithText(descendantChildrenElementClassName, null, eleventhAction);
			//Step 3
			driverUtil.verifyToastText(eleventhTextView, eleventhAction + "_Toast.png", "Eleventh clicked");
		} );
		driverUtil.executeWithFatalError(action);

		// Step 4
		action = new ActionDTO("Show as Grid", () -> {
			WebElement showAsGrid = driver.findElementById("com.example.android.adaptertransition:id/action_toggle");
			showAsGrid.click();
		} );
		driverUtil.executeWithFatalError(action);

		String tenthAction = "Tenth";
		action = new ActionDTO(tenthAction, () -> {
			// Step 5
			WebElement tenthTextView = driverUtil.findElementInListThatContainsElementWithText(descendantChildrenElementClassName, null, tenthAction);
			// Step 6
			driverUtil.verifyToastText(tenthTextView, tenthAction + "_Toast.png", "Tenth clicked");
		} );
		driverUtil.executeWithFatalError(action);

	}
	
	@Test
	public void randomItemsTest(){
		WebElement listView = driver.findElement(By.id(absListViewId));
		String childrenClassName = "android.widget.RelativeLayout";
		List<WebElement> listItems = listView.findElements(By.className(childrenClassName));

		Set<Integer> randomListItems = getRandomIntegersInInterval(0, listItems.size() - 1, LIST_ITEMS_TO_TEST);
		for (Integer index : randomListItems) {
			action = new ActionDTO("Item: " + index, () -> {
				WebElement currentListItemTested = listItems.get(index);
				WebElement textViewInCurrentChild = currentListItemTested.findElement(By.className(descendantChildrenElementClassName));
				String listItemText = textViewInCurrentChild.getText();
				driverUtil.verifyToastText(currentListItemTested, listItemText + "_Toast.png", listItemText + " clicked" );
			} );
			driverUtil.execute(action);
		}
		
	}

	private Set<Integer> getRandomIntegersInInterval(int min, int max, int size) {
		Set<Integer> randomNumbers = new HashSet<Integer>();
		for (int i = 0; i < size; i++) {
			int randomNumber = randomInteger(min, max);
			if (!randomNumbers.contains(randomNumber)) {
				randomNumbers.add(randomNumber);
			} else {
				i--;
				continue;
			}
		}
		return randomNumbers;
	}

	// return Integer between min and max, inclusive.
	private int randomInteger(int min, int max) {
		Random rand = new Random();
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}

}
