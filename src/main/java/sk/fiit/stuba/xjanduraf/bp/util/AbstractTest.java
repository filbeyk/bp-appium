package sk.fiit.stuba.xjanduraf.bp.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import sk.fiit.stuba.xjanduraf.bp.settings.MyCapabilities;

public class AbstractTest {
	public static final String SERVER_ADDRESS = "http://127.0.0.1:4723/wd/hub";
	
	protected AndroidDriver driver;
	protected AndroidDriverUtil driverUtil;
	final protected DesiredCapabilities capabilities = new MyCapabilities();
	protected List<ActionDTO> actions = new ArrayList<ActionDTO>();
	protected ActionDTO action;
	
	public void createDriver() throws MalformedURLException{
		driver = new AndroidDriver(new URL(SERVER_ADDRESS), capabilities);
		driverUtil = new AndroidDriverUtil(driver,actions);
	}
	

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
	
}
