package sk.fiit.stuba.xjanduraf.bp.samples;

import java.io.File;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import sk.fiit.stuba.xjanduraf.bp.util.AbstractTest;

public class ITransitTest extends AbstractTest{

	@Before
	public void setUp() throws Exception {
		// set up appium
		// final File classpathRoot = new File(System.getProperty("user.dir"));
		final File appDir = new File("/mnt/WINDOWS/ZDIELANE/owncloud/Bakalarka/Demo_Apps/iTransit/iTransitCommon/app/build/outputs/apk");
		final File app = new File(appDir, "app-debug.apk");

		capabilities.setCapability("app", app.getAbsolutePath());
		capabilities.setCapability("appPackage", "com.apptives.itransit.common");
		capabilities.setCapability("appActivity", ".SplashActivity");
		// capabilities.setCapability("ignoreUnimportantViews", true);
		// http://appium.io/slate/en/master/?ruby#android-only

		createDriver();
	}


	@Test
	public void test() {
		WebDriverWait el = new WebDriverWait(driver, 10);
		el.until(ExpectedConditions.presenceOfElementLocated(By.name("Route")));
	}

    public static String caseInsensitiveXpathTextCondition(String value) {
        return "translate(normalize-space(@text),'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=" +
                escapeQuotes(value.toUpperCase());
    }

	private static String escapeQuotes(String toEscape) {
		return String.format("\"%s\"", toEscape);
	}
}
