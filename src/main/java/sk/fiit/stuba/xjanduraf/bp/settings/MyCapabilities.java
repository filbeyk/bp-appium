package sk.fiit.stuba.xjanduraf.bp.settings;

import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

@SuppressWarnings("serial")
public class MyCapabilities extends DesiredCapabilities {

	private static String platformmName = "Android";
	private static String deviceName = "Nexus5";
	// private static String deviceName = "asus-me371g-Medfield5287DE67";
	private static String platformVersion = "5.1.1";
	// private static String platformVersion = "4.1.2";
	private static String browserName = "Android";

	public MyCapabilities() {
		this.setCapability(CapabilityType.BROWSER_NAME, getBrowserName());
		this.setCapability("platformName", getPlatformName());
		this.setCapability("deviceName", getDeviceName());
		this.setCapability("platformVersion", getPlatformVersion());
		this.setCapability("newCommandTimeout", 300);
		this.setCapability("deviceReadyTimeout", 1);
	}

	public String getBrowserName() {
		return browserName;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public String getPlatformName() {
		return platformmName;
	}

	public String getPlatformVersion() {
		return platformVersion;
	}

}
