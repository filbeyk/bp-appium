package sk.fiit.stuba.xjanduraf.bp.util;

import java.sql.Timestamp;

public class ActionDTO {
	
	private String timeStampId;
	private String errorLog;
	private String screenShot;
	private String pageSource;
	private Runnable action;
	private String name;
	
	public ActionDTO(String nameOfAction,Runnable run){
		timeStampId = new Timestamp(System.currentTimeMillis()).toString();
		this.name = nameOfAction;
		this.action = run;
	}
	public String getTimeStampId() {
		return timeStampId;
	}
	public void setTimeStampId(String timeStampId) {
		this.timeStampId = timeStampId;
	}
	public String getErrorLog() {
		return errorLog;
	}
	public void setErrorLog(String errorLog) {
		this.errorLog = errorLog;
	}
	public String getScreenShot() {
		return screenShot;
	}
	public void setScreenShot(String screenShot) {
		this.screenShot = screenShot;
	}
	public String getPageSource() {
		return pageSource;
	}
	public void setPageSource(String pageSource) {
		this.pageSource = pageSource;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Runnable getAction() {
		return action;
	}
	public void setAction(Runnable action) {
		this.action = action;
	}
	
	
}
