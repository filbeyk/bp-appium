package sk.fiit.stuba.xjanduraf.bp.testCases.actionbar;

import java.io.File;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import sk.fiit.stuba.xjanduraf.bp.util.AbstractTest;
import sk.fiit.stuba.xjanduraf.bp.util.ActionDTO;

public class DoneBarTest extends AbstractTest {

	@Before
	public void setUp() throws Exception {
		final File appDir = new File("/home/janko/Downloads/android-sdk/samples/android-21/ui/DoneBar/Application/" + "build/outputs/apk");
		final File app = new File(appDir, "Application-debug.apk");
		capabilities.setCapability("app", app.getAbsolutePath());
		capabilities.setCapability("appPackage", "com.example.android.donebar");
		capabilities.setCapability("appActivity", ".MainActivity");
		createDriver();
	}
	
	@Test
	public void adapeterTransitionTest() {
		
		
		WebElement doneTextview = driverUtil.findDescendantElement("*", "com.example.android.donebar:id/actionbar_done", "android.widget.TextView");
		driverUtil.verifyTextInElement(doneTextview, "Done");
		//TODO pridat do action baru dalsi element
		//TODO ukazat pristup cez by.classname().get(index) po pridani tohoto elementu

		action = new ActionDTO("Obrazovka", () -> {
			WebElement appName = driverUtil.findElementInActionBar("DoneBar");
			Assert.assertNotNull(appName);
		} );
		driverUtil.executeWithFatalError(action);

		String absListViewId = "android:id/list";

		String descendantChildrenElementClassName = "android.widget.TextView";
		String doneButton = "Done Button";
		action = new ActionDTO(doneButton, () -> {
			WebElement doneBarTextView = driverUtil.findElementInListThatContainsElementWithText(descendantChildrenElementClassName, absListViewId, doneButton);
			doneBarTextView.click();
		} );
		driverUtil.executeWithFatalError(action);
		
		action = new ActionDTO("ActionBar Done", ()-> {
			WebElement doneButtonInActionBar = driverUtil.findElementInActionBar("Done");
			doneButtonInActionBar.click();
		});
		driverUtil.executeWithFatalError(action);
		
		String doneBar = "Done Bar";
		action = new ActionDTO(doneBar, () -> {
			WebElement doneBarTextView = driverUtil.findElementInListThatContainsElementWithText(descendantChildrenElementClassName, absListViewId, doneBar);
			doneBarTextView.click();
		} );
		driverUtil.executeWithFatalError(action);
		
		action = new ActionDTO("ActionBar Cancel", ()-> {
			WebElement doneButtonInActionBar = driverUtil.findElementInActionBar("Cancel");
			doneButtonInActionBar.click();
		});
		driverUtil.executeWithFatalError(action);
	}

}
