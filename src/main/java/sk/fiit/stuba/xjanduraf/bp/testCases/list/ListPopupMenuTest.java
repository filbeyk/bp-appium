package sk.fiit.stuba.xjanduraf.bp.testCases.list;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import sk.fiit.stuba.xjanduraf.bp.util.AbstractTest;
import sk.fiit.stuba.xjanduraf.bp.util.ActionDTO;

//Test LIST, SWIPE (bottom, top)
public class ListPopupMenuTest extends AbstractTest {

	private static final int LIST_ITEMS_TO_TEST = 5;
	

	// list should have ID that will not change during development
	private static final String listId = "android:id/list";
	private static final String descendantChildrenElementClassName = "android.widget.TextView";
	private static final String childrenClassName = "android.widget.LinearLayout";

	// set up appium
	@Before
	public void setUp() throws Exception {
		final File appDir = new File("/home/janko/Downloads/android-sdk/samples/android-21/ui/ActionBarCompat-ListPopupMenu/Application/" + "build/outputs/apk");
		final File app = new File(appDir, "Application-debug.apk");
		capabilities.setCapability("app", app.getAbsolutePath());
		capabilities.setCapability("appPackage", "com.example.android.actionbarcompat.listpopupmenu");
		capabilities.setCapability("appActivity", ".MainActivity");
		createDriver();
	}

	@Test
	public void listViewTest() throws IOException, InterruptedException {

		//Step 1
		action = new ActionDTO("Obrazovka", () -> {
			WebElement appName = driverUtil.findElementByTextCaseInsensitive("ActionBarCompat-ListPopupMenu");
			Assert.assertNotNull(appName);
		} );
		driverUtil.execute(action);
		
		String aragon = "Aragon";
		action = new ActionDTO(aragon, () -> {
			//Step 2
			WebElement aragonTextView = driverUtil.findElementInListThatContainsElementWithText(descendantChildrenElementClassName, listId, aragon);
			//Step 3
			driverUtil.verifyToastText(aragonTextView, aragon + "_Toast.png", "Item clicked: " + aragon);
			//Step 4
			removeItemFromList(driverUtil.findElementInListThatContainsElementWithText(childrenClassName, listId, aragon));
			//Step 5
			verifyRemoveElementInCurrentLayout(listId, descendantChildrenElementClassName, childrenClassName, aragon);
		} );
		driverUtil.executeWithFatalError(action);

		String acorn = "Acorn";
		action = new ActionDTO(acorn, () -> {
			//Step 6
			WebElement acornTextView = driverUtil.findElementInListThatContainsElementWithText(descendantChildrenElementClassName, listId, acorn);
			//Step 7
			driverUtil.verifyToastText(acornTextView, acorn + "_Toast.png", "Item clicked: " + acorn);
		} );
		driverUtil.executeWithFatalError(action);
	}
	
	@Test
	public void randomItemsTest(){
		WebElement listView = driver.findElement(By.id(listId));
		List<WebElement> listItems = listView.findElements(By.className(childrenClassName));

		Set<Integer> randomListItems = getRandomIntegersInInterval(0, listItems.size() - 1, LIST_ITEMS_TO_TEST);
		for (Integer index : randomListItems) {
			action = new ActionDTO("Item: " + index, () -> {
				WebElement currentListItemTested = listItems.get(index);
				WebElement textViewInCurrentChild = currentListItemTested.findElement(By.className(descendantChildrenElementClassName));
				String listItemText = textViewInCurrentChild.getText();
				driverUtil.verifyToastText(currentListItemTested, listItemText + "_Toast.png", "Item clicked: " + listItemText);
//				removeItemFromList(currentListItemTested);
				// pass to function updated list of elements
//				verifyRemoveElementInCurrentLayout(listId, descendantChildrenElementClassName, childrenClassName, listItemText);
			} );
			driverUtil.execute(action);
		}
		
	}

	private void verifyRemoveElementInCurrentLayout(String listId, String descendantChildrenElementClassName, String childrenClassName, String listItemText) {
		boolean containsRemovedElement = containsItemWithText(
				driver.findElements(By.xpath("//*[@resource-id='" + listId + "']/" + childrenClassName + "/" + descendantChildrenElementClassName)),
				listItemText);
		Assert.assertEquals("Removed element with text: '" + listItemText + "' is still presented in list.", false, containsRemovedElement);
	}

	private void removeItemFromList(WebElement currentLinearLayoutTested) {
		WebElement popupMenu = currentLinearLayoutTested.findElement(By.className("android.widget.ImageView"));
		popupMenu.click();
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		driverUtil.findElementByTextCaseInsensitive("remove").click();
	}

	private boolean containsItemWithText(List<WebElement> listItems, String text) {
		for (WebElement uiElement : listItems) {
			if (uiElement.getText().equalsIgnoreCase(text)) {
				return true;
			}
		}
		return false;
	}

	private Set<Integer> getRandomIntegersInInterval(int min, int max, int size) {
		Set<Integer> randomNumbers = new HashSet<Integer>();
		for (int i = 0; i < size; i++) {
			int randomNumber = randomInteger(min, max);
			if (!randomNumbers.contains(randomNumber)) {
				randomNumbers.add(randomNumber);
			} else {
				i--;
				continue;
			}
		}
		return randomNumbers;
	}

	// return Integer between min and max, inclusive.
	private int randomInteger(int min, int max) {
		Random rand = new Random();
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}

}
