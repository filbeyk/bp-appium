package sk.fiit.stuba.xjanduraf.bp.testCases.swipe;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;
import sk.fiit.stuba.xjanduraf.bp.util.AbstractTest;
import sk.fiit.stuba.xjanduraf.bp.util.AndroidDriverUtil;
import sk.fiit.stuba.xjanduraf.bp.util.SwipeDuration;

//Test SWIPE (left, right)
public class ShareActionProviderTest extends AbstractTest{

	@Before
	public void setUp() throws Exception {

		// uncomment to install app to device
		final File appDir = new File("/home/janko/Downloads/android-sdk/samples/android-21/ui/ActionBarCompat-ShareActionProvider/Application/" + "build/outputs/apk");
		final File app = new File(appDir, "Application-debug.apk");
		capabilities.setCapability("app", app.getAbsolutePath());
		capabilities.setCapability("appPackage", "com.example.android.actionbarcompat.shareactionprovider");
		capabilities.setCapability("appActivity", ".MainActivity");
	}

	@Test
	public void listViewTest() throws IOException, InterruptedException {

		// Step 3.1
		WebElement appName = driverUtil.findElementThatContainsCaseInsensitiveText("ActionBarCompat");
		Assert.assertNotNull(appName);

		// Step 3.2
		// list should have ID that will not change during development
		String pagerId = "com.example.android.actionbarcompat.shareactionprovider:id/viewpager";
		WebElement viewPager = driver.findElement(By.id(pagerId));

		
		driverUtil.swipeFastToRight(viewPager);
		
		// Step 3.3
		Set<Integer> imageIndexedFromRight = new HashSet<Integer>();
		imageIndexedFromRight.add(0);
		imageIndexedFromRight.add(2);
		imageIndexedFromRight.add(5);
		
		Map<Integer,String> stringIdsIndexedFromRight = new HashMap<Integer, String>();
		stringIdsIndexedFromRight.put(1, "quote_3");
		stringIdsIndexedFromRight.put(3, "quote_2");
		stringIdsIndexedFromRight.put(4, "quote_1");
		
		int index = 0;
		boolean swipeToLeftPossible = true;
		while(swipeToLeftPossible){
			WebElement uiElement; 
			if(stringIdsIndexedFromRight.containsKey(index)){
				String stringId = stringIdsIndexedFromRight.get(index);
				uiElement = driverUtil.findElementByStringId(stringId);
				Assert.assertNotNull(uiElement);
			} else {
				//ImageViews are not in structure
//				uiElement = driverUtil.getElementFromList(pagerId, "android.widget.ImageView", 0, "android.widget.ImageView");
			}
			swipeToLeftPossible = driverUtil.swipeLeftViewPage(viewPager, SwipeDuration.NORMAL);
		}
	}
}